﻿using UnityEngine;

public interface IExplodable
{
    void Exploded();
}
