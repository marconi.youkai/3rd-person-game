﻿using System;
using TMPro;
using UnityEditor.Experimental.Rendering;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerSettings))]
public class PlayerController : MonoBehaviour, ITakeDamage
{
    private static PlayerController _instance;

    public static PlayerController Instance => _instance;
    private PlayerSettings playerSettings;

    private float invTimer = 2.0f;
    private float maxInvTime = 2.0f;
    public int groundContactPoints { get; set; }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        playerSettings = PlayerSettings.Instance;
    }

    public enum ArrowType
    {
        NormalArrow,
        BombArrow,
        TeleportArrow
    }

    public enum ExistingStates : int
    {
        Disabled,
        Dead,
        Grounded,
        Running,
        Dodging,
        Airborn,
        SecondJump,
        Aiming,
        AirborneAiming
    }

    private PlayerState[] _playerStates;
    private int CurrentStateIndex = -1;
    public int lastStateIndex { get; private set; }

    private void Start()
    {
        playerSettings = PlayerSettings.Instance;

        if (playerSettings.PlayerRb != null)
        {
            _playerStates = new PlayerState[9];
            _playerStates[(int) ExistingStates.Disabled] = new DisabledState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.Dead] = new DeadState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.Grounded] = new GroundedState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.Running] = new RunningState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.Dodging] = new DodgeState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.Airborn] = new AirborneState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.SecondJump] = new SecondJump(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.Aiming] = new AimingState(this.playerSettings.PlayerRb);
            _playerStates[(int) ExistingStates.AirborneAiming] = new AirborneAimingState(this.playerSettings.PlayerRb);
            CurrentStateIndex = (int) ExistingStates.Grounded;
        }
    }

    private void Update()
    {
        _playerStates[CurrentStateIndex].Tick();
        playerSettings.DrawForceText.text = playerSettings.drawForce.ToString();

        if (playerSettings.PlayerHealth <= 0)
        {
            StateChanger((int) ExistingStates.Dead);
            return;
        }

        if (CurrentStateIndex != (int) ExistingStates.Grounded)
        {
            return;
        }
        else if (groundContactPoints < 1)
        {
            StateChanger((int) PlayerController.ExistingStates.Airborn);
        }


        if (invTimer <= maxInvTime)
        {
            invTimer += Time.deltaTime;
        }
    }

    private void FixedUpdate()
    {
        _playerStates[CurrentStateIndex].FixedTick();
    }

    public void takeDamage(int healthAmount)
    {
        if (healthAmount == 0)
            return;

        if (healthAmount > playerSettings.PlayerMaxHealth)
        {
            playerSettings.PlayerHealth = playerSettings.PlayerMaxHealth;
            return;
        }

        if (invTimer >= maxInvTime)
        {
            playerSettings.PlayerHealth += healthAmount;
            invTimer = 0.0f;
        }

        Debug.Log("updated Health to : " + playerSettings.PlayerHealth);
    }

    public void StateChanger(int nextStateIndex)
    {
        if (nextStateIndex < 0 || nextStateIndex > _playerStates.Length)
            return;
        lastStateIndex = CurrentStateIndex;
        _playerStates[CurrentStateIndex].OnStateExit();
        CurrentStateIndex = nextStateIndex;
        _playerStates[CurrentStateIndex].OnStateEnter();
    }

    public void StateReturn()
    {
        _playerStates[CurrentStateIndex].OnStateExit();
        CurrentStateIndex = lastStateIndex;
        _playerStates[CurrentStateIndex].OnStateEnter();
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawRay(playerSettings.PlayerTrans.position, playerSettings.PlayerRb.velocity);
    }

#endif
}