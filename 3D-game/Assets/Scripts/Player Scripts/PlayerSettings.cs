﻿using System;
using TMPro;
using UnityEditor;
using UnityEditor.Experimental.Rendering;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;
using Cinemachine;
using DefaultNamespace;

public class PlayerSettings : MonoBehaviour
{
    private static PlayerSettings _instance;

    public static PlayerSettings Instance { get { return _instance; } }
    private LevelManager _levelManager;
    private CanvasController _canvas;
    private bool cursorLock = true;

//    [SerializeField] private BoxCollider feetCollider;
//    public BoxCollider FeetCollider { get { return feetCollider; } }

    #region player settings and properties

    private static Rigidbody playerRB;

    private static Transform playerTrans;

//    [SerializeField] private Transform bodyTrans;
    private int playerHealth = 100;
    private const int PLAYER_MAX_HEALTH = 100;
    public int PlayerMaxHealth { get { return PLAYER_MAX_HEALTH; } }

    [Header("Player movement settings")] [SerializeField, Range(0f, 100f)]
    private float acceleration = 5f;

    [SerializeField, Range(0f, 100f)] private float maxSpeed = 5.0f;
    [SerializeField, Range(0f, 1000f)] private float jumpForce = 5.0f;
    [SerializeField, Range(0f, 100f)] public float runningSpeed = 2.0f;
    [SerializeField, Range(0f, 100f)] public float dodgeForce = 10.0f;
    [SerializeField] public LayerMask groundLayer;

    public Rigidbody PlayerRb { get => playerRB; }

    public Transform PlayerTrans { get => playerTrans; }
//    public Transform BodyTrans { get => bodyTrans; }

    public int PlayerHealth
    {
        get { return playerHealth; }
        set
        {
            if (value >= 0.0f || value <= PLAYER_MAX_HEALTH)
            {
                playerHealth = value;
            }
        }
    }

    public float Acceleration
    {
        get => acceleration;
        private set
        {
            if (value > 0) acceleration = value;
        }
    }

    public float MaxSpeed
    {
        get => maxSpeed;
        private set
        {
            if (value > 0) maxSpeed = value;
        }
    }

    public float JumpForce
    {
        get => jumpForce;
        private set
        {
            if (value > 0) jumpForce = value;
        }
    }

    #endregion


    #region Bow Settings an properties

    [Header("Bow settings")] [SerializeField]
    private ArrowPool arrowPooler;

    [SerializeField] private BombArrowPool bombArrowPooler;
    [SerializeField] private float baseDrawforce = 5f;
    [SerializeField] private float maxDrawForce = 45f;
    [SerializeField] private float drawForceScale = 0;
    [SerializeField] public TextMeshProUGUI DrawForceText;
    [HideInInspector] public bool drewArrow = false;
    [HideInInspector] public Rigidbody arrowrgbd;
    [HideInInspector] public GameObject Arrow = null;
    [HideInInspector] public PlayerController.ArrowType arrowType = PlayerController.ArrowType.NormalArrow;
    public float drawForce = 5f;


//    [SerializeField] private GameObject arrowPrefab;
//    [SerializeField] private GameObject bombArrow;

    public float DrawForceScale { get { return drawForceScale; } private set { drawForceScale = value; } }

    public float MaxDrawForce { get { return maxDrawForce; } set { maxDrawForce = value; } }

    public float BaseDrawforce { get { return baseDrawforce; } set { baseDrawforce = value; } }

    public ArrowPool _arrowPooler { get => arrowPooler; }
    public BombArrowPool _BombArrowPooler { get => bombArrowPooler; }

    public float DrawForce
    {
        get { return drawForce; }
        set
        {
            if (value > 0)
            {
                drawForce = value;
            }
        }
    }

    #endregion


    #region camera variables

    [Header("camera Settings")] [SerializeField]
    private Camera gameCamera;

//    public Camera GameCamera { get { return gameCamera; } }
    public Transform CamTrans { get { return gameCamera.transform; } }
    [SerializeField] public Transform cameraAim { get; private set; }
    [SerializeField] private CinemachineFreeLook _cinemachine3rdPersonCAmera;
    public CinemachineFreeLook Cinemachine3rdPersonCAmera { get { return _cinemachine3rdPersonCAmera; } }
    [SerializeField] private CinemachineVirtualCamera _cinemachineShoulderCamera;
    public CinemachineVirtualCamera CinemachineShoulderCamera { get { return _cinemachineShoulderCamera; } }

    #endregion


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        playerRB = this.GetComponent<Rigidbody>();
        playerTrans = playerRB.transform;
        gameCamera = GameObject.FindObjectOfType<Camera>();
        cameraAim = GameObject.Find("realCameraAim").transform;
        playerRB.freezeRotation = true;
        if (cursorLock == true)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    private void Start()
    {
        _levelManager = LevelManager.Instance;
        _canvas = _levelManager._CanvasController;
        _cinemachine3rdPersonCAmera = GameObject.FindObjectOfType<CinemachineFreeLook>();
        _cinemachineShoulderCamera = GameObject.FindObjectOfType<CinemachineVirtualCamera>();

        arrowPooler = GameObject.FindObjectOfType<ArrowPool>();
        bombArrowPooler = GameObject.FindObjectOfType<BombArrowPool>();
    }
}