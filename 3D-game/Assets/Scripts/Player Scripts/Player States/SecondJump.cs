﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondJump : AirborneState
{
    private PlayerSettings PlayerSettings;

    public SecondJump(Rigidbody playerRb) : base(playerRb)
    {
        this.playerSettings = base.playerSettings;
    }

    public override void Tick()
    {
        deltatime = Time.deltaTime;
        if (playerRB.velocity.y < 0)
        {
            if (Physics.Raycast(playerRB.position, Vector3.down, 1.5f, 512))
            {
                _playerController.StateChanger(IsPlayerRunning());
            }
        }
        
        playerRB.AddForce(gravity * gravityScale, ForceMode.Acceleration);

        if (Input.GetMouseButtonDown(1))
            _playerController.StateChanger((int) PlayerController.ExistingStates.AirborneAiming);
        }
}