﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.Rendering;
using UnityEngine;

public class GroundedState : PlayerState
{
    public GroundedState(Rigidbody playerRB) : base(playerRB) { }

    public override void Tick()
    {
        base.Tick();
        base.AimBow();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            _playerController.StateChanger((int)PlayerController.ExistingStates.Airborn);
            return;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            _playerController.StateChanger((int)PlayerController.ExistingStates.Running);
        }
        
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
           dodge();
        }
        
        
    }

    public override void FixedTick()
    {
        Move();
    }
}