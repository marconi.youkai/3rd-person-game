﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine.Utility;
using UnityEngine;

public class AimingState : GroundedState
{
    protected float mouse_X, mouse_Y, lastMouse_X, mouseAcceleration = 10f;
    private float minAngleY = -70f;
    private float maxAngleY = 70f;
    private Quaternion Xrotation;
    private Camera cam;

    public AimingState(Rigidbody playerRB) : base(playerRB) { }


    public override void OnStateEnter()
    {
        if (!Input.GetMouseButton(1))
        {
            _playerController.StateChanger((int) PlayerController.ExistingStates.Grounded);
        }

        base.OnStateEnter();
        cam = camTransf.GetComponent<Camera>();
        _cinemachineShoulderCamera.m_Priority = 11;
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        _cinemachineShoulderCamera.m_Priority = 0;
    }

    public override void Tick()
    {
        deltatime = Time.deltaTime;
        base.Tick();
        ChangeToShoulderCamera();

        playerTrgt = cameraAim - playerSettings.PlayerTrans.position;
        playerSettings.PlayerTrans.rotation = Quaternion.LookRotation(new Vector3(playerTrgt.x, 0.0f, playerTrgt.z));
        if (Input.GetMouseButtonUp(1))
        {
            _playerController.StateChanger((int) PlayerController.ExistingStates.Grounded);
        }
    }

    public override void FixedTick()
    {
        this.Move();
    }

    protected override void Move(float speedMultiplier = 1)
    {
        if (direction != Vector3.zero)
        {
            playerRB.AddForce(direction, ForceMode.Impulse);
            this.TurnPlayer();
        }
    }


//    protected override void Move()
//    {
//        if (direction != Vector3.zero)
//        {
//            playerRB.AddForce(direction, ForceMode.Impulse);
//            TurnPlayer();
//        }
//    }


    protected new void TurnPlayer()
    {
        playerSettings.PlayerTrans.rotation = Quaternion.Slerp(playerSettings.PlayerTrans.rotation, camTransf.rotation, 0.3f);
    }

    protected override void ChangeToShoulderCamera() { }

    protected static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }

            if (angle > 360F)
            {
                angle -= 360F;
            }
        }

        return Mathf.Clamp(angle, min, max);
    }
}