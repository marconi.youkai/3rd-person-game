﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.Rendering;
using UnityEngine;

public class DodgeState : PlayerState
{
    private float dodgeTime = 0;
    public DodgeState(Rigidbody playerRB) : base(playerRB) { }

    public override void Tick()
    {
        deltatime = Time.deltaTime;

        RotatePlayer(0.8f);
        
        if (dodgeTime >= 0.5f)
        {
            dodgeTime = 0.0f;
            _playerController.StateReturn();
        }

        dodgeTime += deltatime;
    }

    public override void FixedTick() { }
}