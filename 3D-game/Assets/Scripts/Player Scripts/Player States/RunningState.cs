﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningState : GroundedState
{
    public RunningState(Rigidbody playerRB) : base(playerRB) { }

    public override void Tick()
    {
        deltatime = Time.deltaTime;
        ReadInput();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            _playerController.StateChanger((int) PlayerController.ExistingStates.Airborn);
            return;
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            _playerController.StateChanger((int) PlayerController.ExistingStates.Grounded);
        }
    }

    public override void FixedTick()
    {
        Move(playerSettings.runningSpeed);
    }
}