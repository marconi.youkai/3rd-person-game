﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.Rendering;
using UnityEngine;

public class AirborneState : PlayerState
{
    protected Vector3 gravity = new Vector3(0f, -9.81f, 0f);

    protected float gravityScale = 6f;


    public AirborneState(Rigidbody playerRB) : base(playerRB) { }

    public override void Tick()
    {
        base.Tick();

        playerRB.AddForce(gravity * gravityScale, ForceMode.Acceleration);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
            _playerController.StateChanger((int)PlayerController.ExistingStates.SecondJump);
        }
        if (Input.GetMouseButtonDown(1))
            _playerController.StateChanger((int)PlayerController.ExistingStates.AirborneAiming);
        
        dodge();
    }

    public override void FixedTick()
    {
        CheckForGround();
    }

    public override void OnStateEnter()
    {
        base.OnStateEnter();
        originalDrag = playerRB.drag;
        playerRB.drag = 0f;
        playerRB.useGravity = false;
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        playerRB.useGravity = true;
        playerRB.drag = originalDrag;
    }


    protected virtual void CheckForGround()
    {
        if (playerRB.velocity.y < 0)
        {
            if (Physics.Raycast(playerRB.position, Vector3.down, 1.5f, 512))
            {
                _playerController.StateChanger(IsPlayerRunning());
            }
        }
    }

    //    protected override void Jump()
    //    {
    ////        playerRB.velocity = new Vector3(playerRB.velocity.x, 0.0f, playerRB.velocity.z);
    ////        playerRB.AddForce(Vector3.up * playerSettings.JumpForce, ForceMode.Impulse);
    //        playerRB.velocity = new Vector3(playerRB.velocity.x, playerSettings.JumpForce, playerRB.velocity.z);

    //        _playerController.StateChanger((int) PlayerController.ExistingStates.secondJump);
    //    }
}