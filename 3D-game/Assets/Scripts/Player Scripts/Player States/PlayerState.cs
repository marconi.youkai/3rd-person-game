﻿using UnityEngine;
using System.Collections;
using Cinemachine;

public abstract class PlayerState
{
    protected Rigidbody playerRB;
    protected PlayerController _playerController;
    protected PlayerSettings playerSettings;
    protected Transform camTransf;
    protected float originalDrag;
    protected float deltatime;
    protected float move_X;
    protected float move_Z;
    protected Vector3 direction;
    protected CinemachineFreeLook _cinemachineFreeLook;
    protected CinemachineVirtualCamera _cinemachineShoulderCamera;

    protected float originalTimeScale = 1f;
    protected float slowDownScale = 0.2f;

    protected float CamMaxSpeedX;
    protected float CamMaxSpeedY;

    protected Transform camerAimTrans;
    protected Vector3 cameraAim;
    protected Vector3 playerTrgt;

    public PlayerState(Rigidbody playerRB)
    {
        this._playerController = playerRB.GetComponent<PlayerController>();
        this.playerSettings = PlayerSettings.Instance;
        this.playerRB = playerRB;
        this.camTransf = playerSettings.CamTrans;
        _cinemachineFreeLook = playerSettings.Cinemachine3rdPersonCAmera;
        _cinemachineShoulderCamera = playerSettings.CinemachineShoulderCamera;
        CamMaxSpeedX = _cinemachineFreeLook.m_XAxis.m_MaxSpeed;
        CamMaxSpeedY = _cinemachineFreeLook.m_YAxis.m_MaxSpeed;
    }

    public virtual void Tick()
    {
        deltatime = Time.deltaTime;
        camerAimTrans = playerSettings.cameraAim;
        cameraAim = camerAimTrans.position;
        playerTrgt = cameraAim - playerSettings.PlayerTrans.position;
        ReadInput();
    }

    public virtual void FixedTick() { }

    public virtual void OnStateEnter()
    {
        RestoreTime();
    }

    public virtual void OnStateExit()
    {
        RestoreTime();
    }

    protected virtual void ReadInput()
    {
        #region Movement inputs

        move_X = Input.GetAxis("Horizontal") * deltatime;
        move_Z = Input.GetAxis("Vertical") * deltatime;
        direction = (move_X * camTransf.right.normalized) + (move_Z * camTransf.forward.normalized);
        direction = direction.normalized * playerSettings.Acceleration;
        direction.y = 0.0f;
        direction = Vector3.ClampMagnitude(direction, playerSettings.MaxSpeed);

        #endregion

        #region Arrow Change inputs

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            playerSettings.arrowType = PlayerController.ArrowType.NormalArrow;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            playerSettings.arrowType = PlayerController.ArrowType.BombArrow;
        }

//        if (Input.GetKeyDown(KeyCode.Alpha3))
//        {
//            playerSettings.arrowType = PlayerController.ArrowType.TeleportArrow;
//        }

        #endregion
    }

    #region movement_functions

    /// <summary>
    /// moves the player towards a direction
    /// </summary>
    /// <param name="speedMultiplier">speed multiplier to scale the velocity of the player</param>
    protected virtual void Move(float speedMultiplier = 1.0f)
    {
        if (direction != Vector3.zero)
        {
            RaycastHit groundHit;
            if (Physics.Raycast(playerSettings.PlayerTrans.position, Vector3.down, out groundHit, 1.5f, playerSettings.groundLayer))
            {
                if (groundHit.normal != Vector3.up)
                {
                    direction = direction + Vector3.Cross(playerSettings.PlayerTrans.forward, groundHit.normal);
                }
            }

            playerRB.AddForce(direction * speedMultiplier, ForceMode.Impulse);


            RotatePlayer(speedMultiplier);
        }
    }


    protected virtual int IsPlayerRunning()
    {
        return Input.GetKey(KeyCode.LeftShift) ? (int) PlayerController.ExistingStates.Running : (int) PlayerController.ExistingStates.Grounded;
    }

    /// <summary>
    /// make player face movement X and Z direction
    /// </summary>
    protected virtual void RotatePlayer(float rotationTimeScale = 5.0f)
    {
        if (playerSettings.PlayerRb.velocity != Vector3.zero)
        {
            playerSettings.PlayerTrans.rotation = Quaternion.Slerp(playerSettings.PlayerTrans.rotation, Quaternion.LookRotation(new Vector3(playerSettings.PlayerRb.velocity.x, 0.0f, playerSettings.PlayerRb.velocity.z)), 1 / rotationTimeScale);
        }
    }

    protected virtual void Jump()
    {
        playerRB.velocity = new Vector3(playerRB.velocity.x, playerSettings.JumpForce, playerRB.velocity.z);
    }


    protected virtual void dodge()
    {
        if (!Input.GetKeyDown(KeyCode.LeftControl))
            return;
        playerRB.AddForce(((Input.GetAxisRaw("Horizontal") * camTransf.right.normalized) + (Input.GetAxisRaw("Vertical") * camTransf.forward.normalized)) * playerSettings.dodgeForce, ForceMode.VelocityChange);
        _playerController.StateChanger((int) PlayerController.ExistingStates.Dodging);
    }

    #endregion

    #region bowShooting_functions

    protected virtual void AimBow(float timeScaleFactor = 1.0f)
    {
        GetArrow();

        if (Input.GetMouseButton(1))
        {
            RaycastHit cameraRaycastHit;
            Physics.Raycast(camTransf.position, camTransf.forward, out cameraRaycastHit, Mathf.Infinity);
            var arrowTrgt = (cameraRaycastHit.point != Vector3.zero ? cameraRaycastHit.point : camTransf.forward.normalized * 250f) - playerSettings.Arrow.transform.position;
            playerSettings.Arrow.transform.rotation = Quaternion.LookRotation(arrowTrgt);
            PullBowString(timeScaleFactor);
            ChangeToShoulderCamera();
            return;
        }

        playerSettings.drawForce = playerSettings.BaseDrawforce;
        ChangeToFreeCamera();
    }

    /// <summary>
    /// request new arrow from arrow pool if none is active
    /// </summary>
    protected void GetArrow()
    {
        if (playerSettings.drewArrow)
        {
            var arrowctrl = playerSettings.Arrow.GetComponent<ArrowController>();
            if (arrowctrl.ReturnArrowType() == playerSettings.arrowType)
            {
                return;
            }
        }

//        playerSettings.Arrow = playerSettings._arrowPooler.GetIdleObject(playerSettings.PlayerTrans.position, playerRB.rotation, playerSettings.PlayerTrans);
        switch (playerSettings.arrowType)
        {
            case PlayerController.ArrowType.NormalArrow:
                if (playerSettings.Arrow != null)
                {
                    ArrowChanger(PlayerController.ArrowType.NormalArrow);
                }

                playerSettings.Arrow = playerSettings._arrowPooler.Get().gameObject;
                break;
            case PlayerController.ArrowType.BombArrow:
                if (playerSettings.Arrow != null)
                {
                    ArrowChanger(PlayerController.ArrowType.BombArrow);
                }

                playerSettings.Arrow = playerSettings._BombArrowPooler.Get().gameObject;
                break;
            default:
                playerSettings.Arrow = playerSettings._arrowPooler.Get().gameObject;
                break;
        }

        playerSettings.Arrow.transform.SetPositionAndRotation(playerSettings.PlayerTrans.position, playerRB.rotation);
        playerSettings.Arrow.transform.SetParent(playerSettings.PlayerTrans);
        playerSettings.Arrow.SetActive(true);
        playerSettings.arrowrgbd = playerSettings.Arrow.GetComponent<Rigidbody>();
        playerSettings.arrowrgbd.interpolation = RigidbodyInterpolation.None;
        playerSettings.drewArrow = true;

        void ArrowChanger(PlayerController.ArrowType arrowType)
        {
            var arrowCtrl = playerSettings.Arrow.GetComponent<ArrowController>();

            if (arrowCtrl.ReturnArrowType() == arrowType)
            {
                return;
            }

            arrowCtrl.ReturnToPool();
        }
    }

    protected virtual void PullBowString(float timeScaleFactor = 1.0f)
    {
        if (Input.GetMouseButton(0))
        {
            if (playerSettings.DrawForce >= playerSettings.MaxDrawForce)
            {
                playerSettings.drawForce = playerSettings.MaxDrawForce;
            }
            else
            {
                playerSettings.DrawForce += playerSettings.DrawForceScale * (deltatime / timeScaleFactor);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            ReleaseArrow();
        }
    }

    protected void ReleaseArrow()
    {
        if (playerSettings.drewArrow && playerSettings.Arrow != null)
        {
            playerSettings.arrowrgbd.isKinematic = false;
            playerSettings.arrowrgbd.collisionDetectionMode = CollisionDetectionMode.Continuous;
            playerSettings.arrowrgbd.detectCollisions = true;
            playerSettings.Arrow.transform.SetParent(null, true);
            playerSettings.arrowrgbd.AddRelativeForce(new Vector3(0f, 0f, playerSettings.DrawForce), ForceMode.Impulse);
            playerSettings.drewArrow = false;
            playerSettings.DrawForce = playerSettings.BaseDrawforce;
        }
    }

    #endregion


    #region Time changing functions

    /// <summary>
    /// activate slowMotion
    /// </summary>
    protected void SlowDownTime()
    {
        MouseAccel();
        Time.timeScale = Mathf.Lerp(Time.timeScale, slowDownScale, 0.1f);
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    /// <summary>
    /// reset time and physics to its default state
    /// </summary>
    protected void RestoreTime()
    {
        MouseAccel();
        Time.timeScale = originalTimeScale;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }

    #endregion

    #region camera changing related functions

    protected virtual void ChangeToShoulderCamera()
    {
        _playerController.StateChanger((int) PlayerController.ExistingStates.Aiming);
    }

    protected virtual void ChangeToFreeCamera() { }

    /// <summary>
    /// Adjusts cinemachine mouse acceleration in order to compensate for time scale difference
    /// </summary>
    protected void MouseAccel()
    {
        _cinemachineFreeLook.m_XAxis.m_MaxSpeed = CamMaxSpeedX / Time.timeScale;
        _cinemachineFreeLook.m_YAxis.m_MaxSpeed = CamMaxSpeedY / Time.timeScale;
    }

    #endregion
}