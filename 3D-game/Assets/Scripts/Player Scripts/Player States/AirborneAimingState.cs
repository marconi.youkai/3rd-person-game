﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirborneAimingState : SecondJump
{
    protected float mouse_X, mouse_Y, lastMouse_X, mouseAcceleration = 10f;
    private float minAngleY = -70f;
    private float maxAngleY = 70f;
    private float timeScale;
    private Quaternion Xrotation;

    public AirborneAimingState(Rigidbody playerRB) : base(playerRB) { }


    public override void OnStateEnter()
    {
        base.OnStateEnter();
        _cinemachineShoulderCamera.m_Priority = 10;
    }

    public override void OnStateExit()
    {
        base.OnStateExit();
        RestoreTime();
        _cinemachineShoulderCamera.m_Priority = 0;
    }

    public override void Tick()
    {
        deltatime = Time.deltaTime;
        if (Input.GetMouseButtonUp(1))
        {
            _playerController.StateReturn();
            return;
        }
        playerRB.AddForce(gravity * gravityScale, ForceMode.Acceleration);
        playerSettings.PlayerTrans.rotation = Quaternion.Euler(0.0f, camTransf.rotation.eulerAngles.y, 0.0f);
        //        playerSettings.PlayerTrans.rotation = Quaternion.Slerp(playerSettings.PlayerTrans.rotation, Quaternion.Euler(0.0f, camTransf.rotation.eulerAngles.y, 0.0f), 0.3f);
        ChangeToShoulderCamera();
        SlowDownTime();
        base.AimBow(Time.timeScale);
    }


    //    protected override void AimBow()
    //    {
    //        GetArrow();
    //
    //        if (Input.GetMouseButton(1))
    //        {
    //            SlowDownTime();
    //            PullBowString(base.slowDownScale);
    //            return;
    //        }
    //        RestoreTime();
    //        _playerController.StateReturn();
    //    }


    protected new void TurnPlayer()
    {
        mouse_X += Input.GetAxis("Mouse X") * mouseAcceleration;
        mouse_Y += Input.GetAxis("Mouse Y") * mouseAcceleration;
        mouse_Y = ClampAngle(mouse_Y, minAngleY, maxAngleY);
        //        CamPivotTrans.RotateAround(playerTrans.position, Vector3.up, mouse_X); //batedeira simulator
        //        CamPivotTrans.RotateAround(playerTrans.position, Vector3.up, mouse_X - lastMouse_X);
        Xrotation = Quaternion.Euler(mouse_Y * -1, mouse_X, 0f);
        //        playerSettings.PlayerTrans.RotateAround(Vector3.up, mouse_X - lastMouse_X);
        //        playerSettings.PlayerTrans.rotation = Xrotation;
        lastMouse_X = mouse_X;
    }

    protected override void ChangeToShoulderCamera() { }

    protected static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }

            if (angle > 360F)
            {
                angle -= 360F;
            }
        }

        return Mathf.Clamp(angle, min, max);
    }
}