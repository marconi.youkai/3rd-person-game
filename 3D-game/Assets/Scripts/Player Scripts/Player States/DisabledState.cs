﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.Rendering.HDPipeline;
using UnityEngine.UIElements;
using Cinemachine;
using UnityEditor.Experimental.Rendering;
using UnityEngine.Timeline;

public class DisabledState : PlayerState
{
    public DisabledState(Rigidbody playerRB) : base(playerRB) { }
    public override void OnStateEnter() { }
    public override void OnStateExit() { }
    public override void Tick() { }
    public override void FixedTick() { }
}