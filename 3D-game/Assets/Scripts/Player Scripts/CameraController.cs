﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform CamPivotTrans;
    private Transform playerTrans;

    private Quaternion pivotOrigRotation;
    private Transform camTrans;

//    private Quaternion Xrotation;
    private Quaternion Yrotation;

    [Header("Camera settings")]
    [SerializeField, Range(-5, 5),
     Tooltip(
         "Deslocamento de camera horizontal partindo do centro do personagem\n\nCamera horizontal offset from player central position")]
    private float cameraTruck = 0.5f;

    [SerializeField, Range(-5, 5),
     Tooltip(
         "Deslocamento de camera vertical partindo do centro do personagem\n\nCamera vertical offset from player central position")]
    private float cameraPedestal = 0.5f;

    [SerializeField, Range(0, 5),
     Tooltip("Distancia da camera partindo do personagem\n\nCamera distance from the player")]
    private float cameraDolly = 1f;

    [SerializeField, Range(-2, 2),
     Tooltip(
         "Velocidade maxima de reajuste de distancia do player\n\nCamera maximum speed to move near or farther than player")]
    private float cameraDollySpeed = 0f;

    [SerializeField, Range(-360, 360),
     Tooltip("Angulo minimo de rotacao vertical da camera\n\nCamera maximum and minimum tilt rotation")]
    private float minAngleY = -70f;

    [SerializeField, Range(-360, 360),
     Tooltip("Angulo maximo de rotacao vertical da camera\n\nCamera maximum and minimum tilt rotation")]
    private float maxAngleY = 70f;

    [SerializeField,
     Tooltip("Usar controles verticais invertidos para camera\n\nUse inverted vertical control for camera")]
    private bool invertY = false;

    [Header("Camera control settings")] [SerializeField, Range(1, 100)]
    private float mouseAcceleration = 10f;

//    [SerializeField, Range(1, 100)] private float mouseSensibilityX = 1f;
//    [SerializeField, Range(1, 100)] private float mouseSensibilit_Y = 1f;

    private float mouse_X;
    private float mouse_Y;
    private float lastMouse_X = 0;
    private float lastMouse_Ys;

    private void Awake()
    {
        if (camTrans != transform)
            camTrans = gameObject.GetComponentInChildren<Camera>().transform;
        if (playerTrans != transform)
            playerTrans = GetComponentInParent<Transform>();
    }

    private void OnValidate()
    {
        if (camTrans != transform)
            camTrans = gameObject.GetComponentInChildren<Camera>().transform;
        camTrans.position = new Vector3(cameraTruck, cameraPedestal, -cameraDolly);
    }

    private void Start()
    {
        pivotOrigRotation = CamPivotTrans.localRotation;
        camTrans = CamPivotTrans.GetComponentInChildren<Camera>().transform;
    }

    private void Update()
    {
        mouse_X += Input.GetAxis("Mouse X") * mouseAcceleration;
        mouse_Y += Input.GetAxis("Mouse Y") * mouseAcceleration;
        mouse_Y = ClampAngle(mouse_Y, minAngleY, maxAngleY);
//        CamPivotTrans.RotateAround(playerTrans.position, Vector3.up, mouse_X); //batedeira simulator
        CamPivotTrans.RotateAround(playerTrans.position, Vector3.up, mouse_X - lastMouse_X); 
        Quaternion Xrotation = Quaternion.Euler(mouse_Y * (invertY ? 1 : -1), mouse_X, 0f);
        CamPivotTrans.localRotation = Xrotation;
        lastMouse_X = mouse_X;
    }

    private static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }

            if (angle > 360F)
            {
                angle -= 360F;
            }
        }

        return Mathf.Clamp(angle, min, max);
    }
}