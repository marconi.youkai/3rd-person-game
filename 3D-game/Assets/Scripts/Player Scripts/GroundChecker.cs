﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    private PlayerController playerCtrl;

    private void Start()
    {
        playerCtrl = this.GetComponentInParent<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            playerCtrl.StateChanger(Input.GetKey(KeyCode.LeftShift) ? (int)PlayerController.ExistingStates.Running : (int)PlayerController.ExistingStates.Grounded);
            playerCtrl.groundContactPoints++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Ground") && playerCtrl.groundContactPoints > 0)
        {
            playerCtrl.groundContactPoints--;
        }
    }
}
