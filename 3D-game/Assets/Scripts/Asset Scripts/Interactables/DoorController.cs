﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Accessibility;

public class DoorController : MonoBehaviour
{
    //private void OnEnable()
    //{
    //    BullsEyeSwitch.OnActivation += OpenDoorRoutine;
    //}

    //private void OnDisable()
    //{
    //    BullsEyeSwitch.OnActivation -= OpenDoorRoutine;
    //}


    public void OpenDoorRoutine()
    {
        Debug.Log("opening Door");
        this.transform.position = new Vector3(0.0f, Mathf.Lerp(this.transform.position.y, -5.5f, Time.deltaTime * 0.3f), 0.0f);
    }

    private IEnumerator OpenDoor()
    {
        while (this.transform.position.y > -5.5f)
        {
            this.transform.Translate(0.0f, Mathf.Lerp(this.transform.position.y, -5.5f, 0.1f), 0.0f);
            yield return null;
        }
    }
}