﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BullsEyeSwitch : MonoBehaviour
{

    public UnityEvent OnActivation;

    private Renderer meshRenderer;

    private void Start()
    {
        meshRenderer = this.gameObject.GetComponent<Renderer>();
        meshRenderer.material.SetColor("_BaseColor", Color.red);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("PlayerArrow"))
        {
            meshRenderer.material.SetColor("_BaseColor", Color.green);
            if (OnActivation != null)
            {
                OnActivation.Invoke();
            }
        }
    }
}