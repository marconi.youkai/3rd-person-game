﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;

public abstract class Hazard : MonoBehaviour
{
    protected virtual void OnCollisionEnter(Collision other)
    {
        ITakeDamage damagedObj = other.gameObject.GetComponent<ITakeDamage>();

        if (damagedObj != null)
        {
            damagedObj.takeDamage(-10);
        }
        
//        if (other.gameObject.CompareTag("Player"))
//        {
//            var playerCtrl = other.gameObject.GetComponent<PlayerController>();
//            playerCtrl.takeDamage(-10);
//        }
    }

    public virtual int ApplyDamage()
    {
        return 1;
    }

    protected virtual void PushVictim() { }
}