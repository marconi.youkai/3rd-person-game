﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : Hazard
{
    protected override void OnCollisionEnter(Collision other)
    {
        GameObject otherObject = null;
        if (other.gameObject.CompareTag("Player"))
        {
            otherObject = other.gameObject;
            var playerCtrl = otherObject.GetComponent<PlayerController>();
            playerCtrl.takeDamage(-10);
        }

        if (otherObject != null)
        {
            otherObject.GetComponent<Rigidbody>().AddForce((Vector3.Normalize(other.transform.position - other.GetContact(0).point)) * 35.0f, ForceMode.Impulse);
        }
    }
}