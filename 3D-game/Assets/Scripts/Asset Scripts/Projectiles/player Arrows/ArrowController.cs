﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DefaultNamespace;
using UnityEditor.Experimental.Rendering;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;
using Quaternion = System.Numerics.Quaternion;

[RequireComponent(typeof(Rigidbody))]
public class ArrowController : MonoBehaviour
{
    [HideInInspector] public ArrowController instance { get; private set; }
    protected GameObject arrow;
    protected Rigidbody arrowRB;
    protected Transform arrowTrans;
    protected Collider arrowCollider;
    protected bool isFlying = true;
    protected PlayerController.ArrowType _arrowType;

    public virtual PlayerController.ArrowType ReturnArrowType()
    {
        this._arrowType = PlayerController.ArrowType.NormalArrow;
        return this._arrowType;
    }


    protected void OnEnable()
    {
        instance = this;
        arrow = this.gameObject;
        arrowRB = this.arrow.GetComponent<Rigidbody>();
        arrowTrans = this.arrow.GetComponent<Transform>();
        arrowCollider = this.arrow.GetComponentInChildren<Collider>();
        StartCoroutine(ArrowRotation());
    }

    protected virtual void OnCollisionEnter(Collision other)
    {
//        isFlying = false;
//        arrowRB.detectCollisions = false;
//        arrowRB.isKinematic = true;
        ActivateArrow(false);
        arrowTrans.SetParent(other.gameObject.transform, true);
    }

    protected virtual IEnumerator ArrowRotation()
    {
        while (isFlying)
        {
            arrowTrans.transform.LookAt(arrowTrans.position + arrowRB.velocity);
            yield return null;
        }

        yield return new WaitForSecondsRealtime(3);
        ActivateArrow(true);
//        isFlying = true;
        arrow.SetActive(false);
        ReturnToPool();
//        arrowRB.isKinematic = true;
//        arrowRB.detectCollisions = false;
//        arrowRB.collisionDetectionMode = CollisionDetectionMode.Discrete;
    }

    protected virtual void ActivateArrow(bool onAir)
    {
        if (onAir)
        {
            isFlying = true;
            arrowRB.isKinematic = true;
            arrowRB.detectCollisions = false;
            arrowRB.collisionDetectionMode = CollisionDetectionMode.Discrete;
        }
        else
        {
            isFlying = false;
            arrowRB.detectCollisions = false;
            arrowRB.isKinematic = true;
        }
    }

    public virtual void ReturnToPool()
    {
        ArrowPool.Instance.ReturnToPool(this);
    }
}