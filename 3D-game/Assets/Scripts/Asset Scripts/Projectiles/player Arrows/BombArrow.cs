﻿using System;
using System.Collections;
using DefaultNamespace;
using UnityEditor.Experimental.Rendering;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BombArrow : ArrowController
{
    [SerializeField, Range(0f, 1500f)] private float explosiveForce = 500f;

    public override PlayerController.ArrowType ReturnArrowType()
    {
        this._arrowType = PlayerController.ArrowType.BombArrow;
        return this._arrowType;
    }

    protected void OnEnable()
    {
        arrow = this.gameObject;
        arrowRB = this.arrow.GetComponent<Rigidbody>();
        arrowTrans = this.arrow.GetComponent<Transform>();
        arrowCollider = this.arrow.GetComponentInChildren<Collider>();
        StartCoroutine(ArrowRotation());
    }

    protected override void OnCollisionEnter(Collision other)
    {

//        Debug.Log(other.gameObject.name);
//        Debug.Break();
        
        base.OnCollisionEnter(other);

        Collider[] explodedColliders = Physics.OverlapSphere(arrowTrans.position, 5f);

        for (int i = 0; i < explodedColliders.Length; i++)
        {
            Rigidbody obj;
            try
            {
               obj = explodedColliders[i].GetComponent<Rigidbody>();
               obj.isKinematic = false;
            obj.AddExplosionForce(explosiveForce, arrowTrans.position, 5f);
            }
            catch (Exception e)
            {
                continue;
            }
        }
    }

    protected override IEnumerator ArrowRotation()
    {
        while (isFlying)
        {
            arrowTrans.transform.LookAt(arrowTrans.position + arrowRB.velocity);
            yield return null;
        }

//        yield return new WaitForEndOfFrame();
        ActivateArrow(true);
//        isFlying = true;
        arrow.SetActive(false);
        ReturnToPool();
//        arrowRB.isKinematic = true;
//        arrowRB.detectCollisions = false;
//        arrowRB.collisionDetectionMode = CollisionDetectionMode.Discrete;
    }

    public override void ReturnToPool()
    {
        BombArrowPool.Instance.ReturnToPool(this);
    }
}