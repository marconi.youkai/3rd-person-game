﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.HDPipeline;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    private static CanvasController _instance;

    public static CanvasController Instance => _instance;


    private LevelManager _levelManager;
    private PlayerSettings _PlayerSettings;

    private static Slider HealthBar;

    private float healthValue
    {
        get { return HealthBar.value; }
        set
        {
            if (value >= 0.0f && value <= _PlayerSettings.PlayerMaxHealth)
            {
                HealthBar.value = value;
            }
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        this._levelManager = LevelManager.Instance;
        this._PlayerSettings = _levelManager._PlayerSettings;
        HealthBar = GetComponentInChildren<Slider>();
        HealthBar.maxValue = _PlayerSettings.PlayerMaxHealth;
        healthValue = _PlayerSettings.PlayerHealth;
        
    }

    private void Update()
    {
        healthValue = _PlayerSettings.PlayerHealth;
    }
}