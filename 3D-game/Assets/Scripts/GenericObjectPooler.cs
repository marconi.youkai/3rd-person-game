﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class GenericObjectPooler<T> : MonoBehaviour where T : Component
{
    [SerializeField] private T prefab;
    [SerializeField] private int poolSize = 10;

    public static GenericObjectPooler<T> Instance { get; private set; }
    private Queue<T> objects = new Queue<T>();

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }

        if (poolSize > 0)
        {
            AddObjects(poolSize);
        }
    }

    public T Get()
    {
        if (objects.Count == 0)
        {
            AddObjects(1);
        }

        return objects.Dequeue();
    }


    public void ReturnToPool(T objectToReturn)
    {
        objectToReturn.gameObject.SetActive(false);
        objects.Enqueue(objectToReturn);
    }

    private void AddObjects(int count)
    {
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                var newObject = GameObject.Instantiate(prefab, this.transform);
                newObject.gameObject.SetActive(false);
                objects.Enqueue(newObject);
            }
        }
    }
}