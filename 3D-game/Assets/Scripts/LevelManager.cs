﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.Rendering;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }

    private static PlayerSettings _playerSettings;
    public PlayerSettings _PlayerSettings => _playerSettings;
    private CanvasController _canvasController;
    public CanvasController _CanvasController => _canvasController;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _canvasController = CanvasController.Instance;
        _playerSettings = PlayerSettings.Instance;
    }

    private void Start()
    {
    }
}