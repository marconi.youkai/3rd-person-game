﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectPooler : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    [SerializeField] private int prefabCount = 5;
    private GameObject pool;
    private Transform poolTrans;
    private GameObject[] objectArray;

    void Start()
    {
        if (prefab == null)
            return;

        pool = new GameObject((prefab.name + " pool"));
        poolTrans = pool.transform;
        poolTrans.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        poolTrans.SetParent(this.gameObject.transform);
        objectArray = new GameObject[prefabCount > 0 ? prefabCount : 5];
        for (int poolPosition = 0; poolPosition < objectArray.Length; poolPosition++)
        {
            objectArray[poolPosition] = GameObject.Instantiate(prefab, poolTrans);
            objectArray[poolPosition].SetActive(false);
        }
    }

    private GameObject FindIdleGameObject()
    {
        int arrayIndex;
        for (arrayIndex = 0; arrayIndex < objectArray.Length; arrayIndex++)
        {
            if (!objectArray[arrayIndex].activeInHierarchy)
            {
                return objectArray[arrayIndex];
            }
        }

        GameObject[] tempArray = new GameObject[prefabCount];
        prefabCount++;
        for (arrayIndex = 0; arrayIndex < objectArray.Length; arrayIndex++)
        {
            tempArray[arrayIndex] = objectArray[arrayIndex];
        }

        objectArray = new GameObject[prefabCount];
        for (arrayIndex = 0; arrayIndex < tempArray.Length; arrayIndex++)
        {
            objectArray[arrayIndex] = tempArray[arrayIndex];
        }

        return objectArray[objectArray.Length - 1] = GameObject.Instantiate(prefab, pool.transform);
    }

    public GameObject GetIdleObject(Vector3 newPosition, Quaternion newRotation, Transform parent)
    {
        GameObject nextGameObject = FindIdleGameObject();
        nextGameObject.SetActive(true);
        nextGameObject.transform.SetParent(parent);
        nextGameObject.transform.SetPositionAndRotation(newPosition, newRotation);
        return nextGameObject;
    }

    public void ReturnToPool(GameObject gameObject)
    {
        for (int i = 0; i < objectArray.Length; i++)
        {
            if (gameObject.activeInHierarchy == false)
            {
                gameObject.transform.SetParent(this.pool.transform, true);
            }
        }
    }
}